### Stretch Goals for FY25 

- ✅ Thought leadership - added blogs & completed Techstrong panel 
- GitLab's CI/CD Certification 
- ✅ Support new metrics for Customer Success - DONE 

### Future priorties - 2023-10-23

- [ ] 

### Next priorties -  2023-10-17

- [ ] Async week
- [ ] GCP <> GitLab Customer Interviews 
- [ ] CDF Review Prep for Team 

### Right now priority - 2023-10-09 to 2020-10-13 

- [ ] CI Section Direction MR Merge - https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/129734 
- [ ] Record CI Direction Walkthrough 
- [ ] GCP <> GitLab Customer Interviews 
- [ ] CDF Review Prep for Team / Talent Assessment 
- [ ] Pipeline Execution Direction Planning for Merge Trains, Continuous Integration, Review Apps, & Code Testing 
- [ ] Refresh decks for FY25 vision 
- [ ] Plan 16.6, 16.7, 16.8, and 16.9 for Pipeline execution

###  2023-10-02 to 2023-10-06
- ✅  Iterate on Product Direction Showcase - Add Issue link 
- ✅  Pipeline Execution PM Hiring 
- ✅  Product Offsite/PLT and Director+ Offsite 

### 2023-09-25 to 2023-09-29

- ❌ CI Section Direction MR Merge - https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/129734
- ✅  GCP Integration Project Meetings
- ✅ Finalize GCP<> GitLab CI Integration PRD 
- ❌  Plan 16.6, 16.7, 16.8, and 16.9 for Pipeline execution
- ✅  PE PM Hiring 
- ✅ Prep for Product Offsite in Denver 
- ✅ Product Direction Showcase 

### 2023-02-06 to 2023-02-10

- [ ] Add MRs to Secrets Management Page based on Research - https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/blob/main/Verify_and_Package_Research_Synthesis.md
✅ OKR Updates and Objective Updates 
✅ Analyst Waves 
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)
- [ ] Research standalone "Build Perfomance" tools/offerings 
- [ ] Iterate on Monthly Solution Syncs - https://gitlab.com/gitlab-com/Product/-/issues/4606
- ✅ Ops Section Direction Update - Add a CI/CD direction page, update Verify Stage direction - https://gitlab.com/gitlab-com/Product/-/issues/5230


### 2023-01-30 to 2023-02-03 

- [ ] Research standalone "Build Perfomance" tools/offerings 
✅ Establish TMRG for Caregivers - https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/756
- [ ] Iterate on Monthly Solution Syncs - https://gitlab.com/gitlab-com/Product/-/issues/4606
- [ ] Ops Section Direction Update - Add a CI/CD direction page, update Verify Stage direction - https://gitlab.com/gitlab-com/Product/-/issues/5230
- [ ] Review Secrets Management Research - https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/blob/main/Verify_and_Package_Research_Synthesis.md
- [ ] Analyst Waves 

### 2023-01-23 to 2023-01-27

✅ Cross-section JTBD mapping - https://gitlab.com/gitlab-com/Product/-/issues/5290
✅ Create CDF goals issues for Q1 - https://gitlab.com/gitlab-com/Product/-/issues/4723 
✅ Update TAM/SAM for Package - https://gitlab.com/gitlab-com/Product/-/issues/5067 
- [ ] Research standalone "Build Perfomance" tools/offerings 
- [ ] Establish TMRG for Caregivers - https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/756
- [ ] Iterate on Monthly Solution Syncs - https://gitlab.com/gitlab-com/Product/-/issues/4606
- [ ] Ops Section Direction Update - Add a CI/CD direction page, update Verify Stage direction - https://gitlab.com/gitlab-com/Product/-/issues/5230
✅ Q1 OKRs - collaborate with Engineering on Product OKRs - https://gitlab.com/gitlab-com/Product/-/issues/5122
- [ ] Review Secrets Management Research - https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/blob/main/Verify_and_Package_Research_Synthesis.md
- [ ] Analyst Waves 


###  2023-01-16 to 2023-01-20 

- [ ] Establish TMRG for Caregivers - https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/756
- [ ] Iterate on Monthly Solution Syncs - https://gitlab.com/gitlab-com/Product/-/issues/4606
- [ ] Ops Section Direction Update - Add a CI/CD direction page, update Verify Stage direction - https://gitlab.com/gitlab-com/Product/-/issues/5230
- [ ] Q1 OKRs - collaborate with Engineering on Product OKRs - https://gitlab.com/gitlab-com/Product/-/issues/5122
✅ Product direction showcase - https://gitlab.com/gitlab-com/Product/-/issues/5301 
✅ Complete Verify Category review - https://gitlab.com/gitlab-com/Product/-/issues/5180
✅ Ops PI Review for Jan - https://gitlab.com/gitlab-com/Product/-/issues/5313
✅ Verify blocked issue review - https://gitlab.com/gitlab-com/Product/-/issues/5324 
✅ DIB Influencer Group Lead Tasks - https://gitlab.com/gitlab-com/people-group/dib-diversity-inclusion-and-belonging/diversity-and-inclusion/-/issues/817 
✅ Review Investment cases/Opportunity canvases across Ops Section
✅ Unblock Verify UX Benchmark - https://gitlab.com/groups/gitlab-org/-/epics/9494
- [ ] Review Secrets Management Research - https://gitlab.com/enf/verify-and-package-pv-research-synthesis/-/blob/main/Verify_and_Package_Research_Synthesis.md
 

### Parental leave - 2022-08-22 to 2022-12-12 - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4225) 

### 2022-08-15 to 2022-08-19
✅ Close out Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ Tranisition any open items for CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
✅  Button up Parental Leave Issue 
✅  Container Registry Migration 
✅  Map over CDFs to new Template per the Opt-in Period 
✅  Set future date on monthly field sync MR 
✅  Make a decision on [SUS Borrow](https://gitlab.com/gitlab-com/Product/-/issues/4527)

### 2022-08-08 to 2022-08-12 
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
- [ ] Merge monthly field sync MR 
- [ ] Make a decision on [SUS Borrow](https://gitlab.com/gitlab-com/Product/-/issues/4527)
✅ SocialTalent Training for Managers
✅ Product Direction Showcase 
✅ Container Registry Migration 
 
 ### 2022-08-01 to 2022-08-05
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
- [ ] Merge Forced Prioritization MR 
✅ Merge Scope Reassignment MR  
✅ CI/CD Pricing research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4167) - Delegate to James 
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)
- [ ] Merge monthly field sync MR 
- [ ] Make a decision on [SUS Borrow](https://gitlab.com/gitlab-com/Product/-/issues/4527)

### 2022-07-25 to 2022-07-29

✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
- [ ] Merge Forced Prioritization MR 
- [ ] Merge Scope Reassignment MR  
- [ ] CI/CD Pricing research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4167) - Delegate to James 
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)
- [ ] Merge monthly field sync MR 
✅ Review canvases for PI and PA 
- [ ] Make a decision on [SUS Borrow](https://gitlab.com/gitlab-com/Product/-/issues/4527)
✅ CI Workflows Blog - [Issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13677)
✅ Ally KRs for [Verify](https://gitlab.com/gitlab-com/Product/-/issues/4366) and [Package](https://gitlab.com/gitlab-com/Product/-/issues/4368) 

### 2022-07-18 to 2022-07-22
✅  Q1 SUS/PNPS Calls 
✅Free User Strategy Sub-project support 
✅Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
✅ July CAB  

### 2022-07-11 to 2022-07-15
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
✅ July CAB Prep 
✅ Finalize OKR planning for FY23-Q3
✅ Add MR for Monthly Field Syncs on Solutions - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4419)  
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)

### 2022-07-04 to 2022-07-08
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
✅ July CAB  
✅ Kick off OKR planning for FY23-Q3 
✅ Verify Team Week
✅ Collateral on improving move to SaaS - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106746/diffs)
- [ ] CI/CD Pricing research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4167)
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)

### 2022-06-27 to 2022-07-01
✅ Q1 SUS/PNPS Calls 
- [ ] Close out Cross-stage research  
✅ Free User Strategy Sub-project support 
- [ ] Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
- ✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
- ✅ July CAB Prep 
- ✅ Kick off OKR planning for FY23-Q3 
- ✅ Verify Team Week
- [ ] CI/CD Pricing research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4167)
- [ ] Developer Experience Pricing Theme [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104730)
✅ Support for teammates [PTO](https://gitlab.com/gitlab-com/Product/-/issues/4316)

### 2022-06-20 to 2022-06-24
✅Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
- [ ] Finish tagging and consolidating findings for Cross-stage research  
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
- [ ] Investment Case for [CI Scaling](https://gitlab.com/gitlab-com/Product/-/issues/4146)
✅ Finish Build Artifact Survey and JTBD definition - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1956)

###   2022-06-13 to 2022-06-17
- [ ] Q1 SUS/PNPS Calls 
- [ ] Free User Strategy Sub-project support 
- [ ] Finish tagging and consolidating findings for Cross-stage research  
✅ Premium Runner Investment case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
✅ Enterprise Leadership Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/enterprise-market-leadership/)
✅ CDF Working Group - [HB Page](https://about.gitlab.com/company/team/structure/working-groups/product-career-development-framework/)
✅ Analyst Briefings for 15 
✅ PLT Offsite Follow-ups 

### 2022-06-06 to 2022-06-10
✅  Q1 SUS/PNPS Calls 
✅  Free User Strategy Sub-project support 
✅  Cross-stage research 
- [ ] Tag research findings, fill out deck, and share insights 
✅ Premium Runner Investment case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
- [ ] GitLab's CI/CD Certification 
✅ June process refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/159)
✅ PLT Offsite 
✅  Review planning issues, MVCs, and Direction Pages 

### 2022-05-30 to 2022-06-03
✅  Q1 SUS/PNPS Calls 
✅  Free User Strategy Sub-project support 
✅  Cross-stage research  
✅  Cross-stage research recruiting 
- [ ] Premium Runner Investment case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
- [ ] GitLab's CI/CD Certification 
✅  Merge updates to CDF working group
✅  Kick off Enterprise Leadership Working Group 
✅  Merge finance partner updates 

✅  May process refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/159)

✅  May Competitive Roadmap Review - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4156)

### 2022-05-23 to 2022-05-27
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Cross-stage research recruiting 
- [ ] Premium Runner Investment case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
✅ GitHub Compete project and Vision - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3937)
✅ Ask product leadership if we want to restablish the product CDF working group 
✅] Attend QBRs - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4070)
- [ ] Finalize Verify Team Day - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/196)
✅ Product Manager CDF reviews
✅ LifeLabs Training 

### 2022-05-16 to 2022-05-20
✅ Q1 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Fully Transition Jocelyn to Pipeline Insights 
✅ Cross-stage research recruiting 
✅ Record Verify Strategy Overview 
✅ Merge MRs to transition Pipeline insights on 2022-05-16 
- [ ] Premium Runner Investment case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
- [ ] Ask product leadership if we want to restablish the product CDF working group 
✅ Attend QBRs - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4070)
- [ ] Finalize Verify Team Day - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/196)
✅ Finalize Product Direction Showcase - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4160)
✅ Kick off GitHub Compete project and Vision - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3937)

### [PTO 2022-05-06 to 2022-05-13 ](https://gitlab.com/gitlab-com/Product/-/issues/4071)

### 2022-05-02 to 2022-05-06
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Acting Pipeline Insights PM 
✅ Support new Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
✅ Finalize Verify FY23-Q2 KRs 
✅ Open MRs to transition Pipeline insights on 2022-05-16 
✅ Attend QBRs - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4070)

###  2022-04-25 to 2022-04-29
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Acting Pipeline Insights PM 
✅ Support new Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
✅ Coach Pipeline Insights PM on First 100 Days Issue and transition tasks over to them 
✅ Finalize and enter in OKRS - [Issue](Draft: Verify FY23-Q2 OKR Planning)
- [ ] Attend QBRs - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4070)
✅ Verify Validation Backlog Review 
✅ Finalize pricing tier for CICD catalog - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/4048)
✅ Finalize [Ultimate Feature Discussion](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101798) 
- [ ] GitLab's CI/CD Certification 

### 2022-04-18 to 2022-04-22
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅Acting Pipeline Insights PM 
✅Support new Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] GitLab's CI/CD Certification 
- [ ] Finalize [Ultimate Feature Discussion](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101798) 
✅ Direction Updates 
✅ Ops Performance Indicator Updates 
✅ Add GH Labels to Verify Backlog Issue - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/198)

###  2022-04-11 to 2022-04-15
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅  Acting Pipeline Insights PM 
✅ Support new Pipeline Insights PM
✅  GitLab Women Mentorship 
✅  Cross-stage research recruiting 
✅  CAB Support - [BoF](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/128) and [Main Deck](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/127)
✅  Discuss Ultimate Features in 2022 - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101798)
✅  Finalize Verify Borrow Request - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3852)
✅ JTBD Survey for Build Artifacts - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3954)
- [ ] GitLab's CI/CD Certification 

### 2022-04-04 to 2022-04-08
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Acting Pipeline Insights PM 
✅ Support new Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] Investment case Premium Runners - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3928)
✅ CAB Support - [BoF](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/128) and [Main Deck](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/127)
✅ Merge issue template and competitive process tracking update - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3986)
✅ Refresh Verify Stage Deck - https://docs.google.com/presentation/d/1yPu7K39TtzCh7VTGk-Am93lzZWcN9_xVeooO1Tgyoys/edit?usp=sharing 
- [ ] Discuss Ultimate Features in 2022 - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/101798)
- [ ] Finalize Verify Borrow Request - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3852)
- [ ] JTBD Survey for Build Artifacts - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3954)
- [ ] Confirm Product Direction Showcase is a desired routine 

###   2022-03-28 to 2022-04-01
✅ Q4 SUS/PNPS Calls 
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Acting Pipeline Insights PM 
✅ Support new Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
✅ Competitive Roadmap Review for March 
- [ ] GitLab's CI/CD Certification 
- [ ] JTBD Survey for Build Artifacts - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3954)
✅ US Bank support - [Issue](https://gitlab.com/gitlab-com/account-management/western-north-america/US-Bank/-/issues/86)
- [ ] Finalize Verify Borrow Request - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3852)
✅ CAB Support - [BoF](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/128) and [Main Deck](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/customer-advisory-board/-/issues/127)

### 2022-03-21 to 2022-03-25
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Cross-stage research  
✅ Acting Pipeline Insights PM 
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] Close SMB cross-stage research 
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
✅ Record Product Led Summit Presentation 
✅ Prep new PI PM First 100 day issue 
- [ ] 15.1 Planning 
✅ Lululemon Support for their Summit 

### 2022-03-14 to 2022-03-18
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Acting Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research  
- [ ] Close SMB cross-stage research 
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
- [ ] Record Product Led Summit Presentation 
✅ Prep new PI PM onboarding issues and select onboarding buddy 
✅ Support US Bank GitLab CI adoption narrative 
✅ CAB Main Session Prep 
✅ Monthly PI updates for Verify & Pipeline Insights 
✅ Resolve Security Borrow for Verify Stage 
✅ Resolve Pipeline Insights error budget challenges 
- [ ] 15.1 Planning 
✅ Revamp Code Testing Direction Page 
✅ Reconsider Pipeline Insights GMAU Timelines 

### 2022-03-07 to 2022-03-11
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Acting Pipeline Insights PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] Close SMB cross-stage research 
✅ CAB Prep and deck creation 
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
✅ March Process Reviews - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/157)
✅ Release post creation 
✅ Resolve [expired artifacts challenges](https://gitlab.com/groups/gitlab-org/-/epics/7097)
✅ Pipeline Insights 14.10 Kick off 
- [ ] Merge Internal Feedback Page to Product Handbook - [MR](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/454)

### 2022-02-28 to 2022-03-04
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Acting Testing PM 
✅ Hiring for Testing PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting
✅ CDF Reviews  
- [ ] Partner with UX leadership on SUS Labels - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98712)
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
- [ ] Close SMB cross-stage research 
✅ Ops Section Direction Review 

###  2022-02-21 to 2022-02-25
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Acting Testing PM 
✅ Hiring for Testing PM
✅ GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] Partner with UX leadership on SUS Labels - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98712)
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
- [ ] Close SMB cross-stage research 
✅ CDF review Prep for team 


### 2022-02-14 to 2022-02-18
✅ PTO on 2022-02-14 
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Acting Testing PM 
✅ Hiring for Testing PM
✅  GitLab Women Mentorship 
✅ Cross-stage research recruiting 
- [ ] Close SMB cross-stage research 
✅ Ops Section Direction Review [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98971)
✅ Ops PI Updates - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3830)
✅ Pipeline Insights Async Think Big - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-insights-group/-/issues/93)
✅ SKO presentation Deck - [Issue]()
- [ ] Verify Stage Deck Refresh - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
✅ GitLab Assembly 
✅ Merge and schedule Product Direction Showcase - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98193)
- [ ] Partner with UX leadership on SUS Labels - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98712)


### 2022-02-07 to 2022-02-11
✅ Q4 SUS/PNPS Calls 

✅ Rapid Action for Free User Strategy Support

✅ Free User Strategy Sub-project support 

✅ Acting Testing PM 

✅ Hiring for Testing PM

✅ GitLab Women Mentorship 

✅ Cross-stage research recruiting 

✅ 14.9 Planning for Testing - [Issue](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/87)

✅ Testing Kick Off

- [ ] Close SMB cross-stage research 

✅ QBRs 

✅ Schedule Verify:Pipeline Insights Think Big 

✅ Update Verify:Pipeline Insights Roadmap 

### Right now priorities - 2022-01-31 to 2022-02-04
- [ ] Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Acting Testing PM 
✅ Hiring for Testing PM
✅ Identify key players/competitors for Load, Performance, Accessibility, Review Apps, and Browser Testing (Open Direction Page MRs)
✅ CI minute PI reassignment/decision - [MR](https://gitlab.com/internal-handbook/internal-handbook.gitlab.io/-/merge_requests/336#note_826906040)
✅ GitLab Women Mentorship 
- [ ] Close SMB cross-stage research 
✅ Open new cross-stage research and review discussion guide 
✅ Verify Roadmap - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3740)
✅ Rename Testing Group - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97760)
✅ Update proposed new CDF with changes and consider second pilot have setting outcomes training - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/96377)
✅ Runner Stable counterpart - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97590)
✅ Verify FY23-Q1 KRs - [Ally](https://app.ally.io/teams/39884/objectives?tab=0&time_period_id=155985&viewId=209698)

###  2022-01-24 to 2022-01-28
✅  Q4 SUS/PNPS Calls 
✅  Rapid Action for Free User Strategy Support
✅  Free User Strategy Sub-project support 
✅  Acting Testing PM 
✅  Hiring for Testing PM
✅  Solidfy Verify Stage KRs 
✅  CDF Working Group decison/direction 

### 2022-01-17 to 2022-01-21
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Acting Testing PM 
✅  Hiring for Testing PM
✅ Testing Team process improvements 
✅ CDF Reviews for PMs 
✅  Secure Supply Chain Vision 
✅ CDF Working Group decison/direction 

###  2022-01-10 to 2022-01-14
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ CDF Working Group training videos and begin roll out 
✅ Pricing Handbook for Product Launches - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅ Acting Testing PM 
✅ Hiring for Testing PM
✅ Testing Kick off 
✅ Product Onboarding consolidation - [MR](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)

### 2022-01-03 to 2022-01-07
✅ Q4 SUS/PNPS Calls 
✅ Rapid Action for Free User Strategy Support
✅ Free User Strategy Sub-project support 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
- [ ] CDF Working Group training videos and begin roll out 
- [ ] Pricing Handbook for Product Launches - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅ Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Acting Testing PM 
✅ Hiring for Testing PM
- [ ] Product Onboarding consolidation - [MR](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)

### 2021-12-27 to 2021-12-31
✅  PTO Week 
✅  Product Vision Sessions with Robbi

### 2021-12-20 to 2021-12-24
✅  Q3 SUS Calls 
- [ ] Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Support Testing PM onboarding 
✅  Product Vision Sessions with Robbi 
✅ PTO 

### 2021-12-13 to 2021-12-17
✅ Q3 SUS Calls 

✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

- [ ] Pricing Handbook for Product Launches - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)

- [ ] Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)

✅  Support Testing PM onboarding 

✅  CDF Reviews for Verify Stage Teammates - [Issues](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=verify%3Acdf)

- [ ] Start Product Vision Sessions with Robbi 

✅  Product Onboarding consolidation - [MR](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)

### 2021-12-06 to 2021-12-10
- PTO week 

### 2021-11-29 to 2021-12-03
✅ Q3 SUS Calls 
- [ ] Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
- [ ] Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Support Testing PM onboarding 
- [ ] Complete Product Onboarding consolidation - [MR](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)
✅ Direction Page Updates - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3436)
✅ Product CDF Working Group - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3152)
✅ Organize Verify Stage Deprecations - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/166)
✅ Create Verify KR Retro issue for Q4
✅ Assign December Process Refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/154)

###  2021-11-22 to 2021-11-26
✅ Q3 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Support Verify:PE PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
✅ Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Support Testing PM onboarding 
✅ Release post manager duties 
✅ Discuss impacts of potential Verify Platform Team - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94301)
✅ Ops PI Review - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3415)
✅ Thanksgiving holiday 

### 2021-11-15 to 2021-11-19
✅ Q3 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Support Verify:PE PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)
- [ ] Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Support Testing PM onboarding 
✅ Release post manager duties 
✅ Free user strategy support of CI/CD minute limits 

### 2021-11-08 to 2021-11-12
✅  Q3 SUS Calls 
✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅  Support Verify:PE PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
✅  CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)
✅  Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅  Support Testing PM onboarding 
✅ Product CDF Working Group 
- [ ] Revamp the onboarding workflows and issues across Product 
✅ Release post manager duties 
✅ Redefine my role as GMP and start focusing on longer term vision 
✅ CI/CD minutes strategy hand off to James 
✅ Publish blog for CI minute changes 

### 2021-11-01 to 2021-11-05
✅ Q3 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Support Verify:PE PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)
✅ Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Support Testing PM start week 
✅ Record CI direction page 
✅ Merge CI Direction V1 
- [ ] Revamp the onboarding workflows and issues across Product 
✅ Support Runner MAU Discussions and Funnel - [Issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/133)
✅ Finalize CI Scaling EA changes and Runner Scaling challenges - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3242) and [CI Scaling EA](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92926)
✅ Product CDF Working Group 

### 2021-10-25 to 2021-10-29
✅ Q3 PNPS Calls 
✅ Q3 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Support Verify:PE PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
✅ Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Prep for Testing PM 
✅ CDF reviews for team 
✅ Analyst calls for Artifact management and cloud hosted CI 

- [ ] Merge CI Direction V1 
- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)

### 2021-10-18 to 2021-10-22 
✅ Q2 PNPS Calls 
✅ Q2 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Support Verify Testing PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3256)
✅  Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
✅ Open plan for Verify Testing PM Onbaording 
✅  Cover Kick off for Ops Section in 14.5 
✅  Autoscaling for Runner Allocation and timing 
✅  Ops PI review Prep - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3246)
✅ Ops direction page updates 
✅ CI Direction Page 

- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)

### 2021-10-12 to 2021-10-15
✅ Q2 PNPS Calls 
✅ Q2 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Support Verify Testing PM Transition plan [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3160)
✅ Move Verify PE onboarding to product project 
✅ CI minutes Blog post - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90763)
✅ Finish planning issues for future milestones through 14.8 
✅ 14.5 kick off video 
✅ Verify Stage Q4 KRs - [Issue](https://gitlab.com/groups/gitlab-org/-/epics/6871)
✅ Update Ally for Q3 KRs 
✅Cross-stage research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)

- [ ] Open plan for Verify Testing PM Onbaording 

### PTO 2021-09-27 to 2021-10-11
- ✅  Coverage [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3080)

### 2021-09-20 to 2021-09-24 
✅ Q2 PNPS Calls 
- [ ] Q2 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Screening calls for Verify:PE PM
✅ Hiring Manager intervies for Verify:PE PM
✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅ Women at GitLab Mentorship Sync 
✅ Weekly SaaS PI Updates 
- [ ] Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅ Ops PI Updates 
✅ 360 Reviews 

### 2021-09-13 to 2021-09-17
- [ ] Q2 PNPS Calls 
- [ ] Q2 SUS Calls 
- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Screening calls for Verify:PE PM
✅ Hiring Manager intervies for Verify:PE PM
✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅ Women at GitLab Mentorship Sync 
✅ Weekly SaaS PI Updates 
✅ Package Vision Review in PE Meeting - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2693)
✅ Reconsider CI Scaling Engineering Allocation Exit Criteria - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336449)
✅ CDF Reviews 

###  2021-09-06 to 2021-09-10
- [ ] Q1 PNPS Calls 
- [ ] Q1 SUS Calls 
✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Screening calls for Verify:PE PM
✅  Hiring Manager intervies for Verify:PE PM
✅  Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅  Women at GitLab Mentorship Sync 
- [ ] Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅  OSS Impact for CI Minutes - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
✅  September Process Refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/98)
✅ CI Direction Page - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/3069)
- [ ] CI Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)
✅  14.5 planning - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/71)

###  2021-08-30 to 2021-09-03
✅ Q1 PNPS Calls 
✅  Q1 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅  Verify PE PM Acting Duties 
✅  Screening calls for Verify:PE PM
✅  Hiring Manager intervies for Verify:PE PM
✅  Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅  Women at GitLab Mentorship Sync 
✅ OSS Impact for CI Minutes - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
✅ Cheryl coverage tasks 
✅  Pipeline Execution Docs Audit - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339455)
✅  CI Use Case Metrics - [Issue](https://gitlab.com/gitlab-com/sales-team/field-operations/customer-success-operations/-/issues/473)
✅  Direction Updates - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2992)
- [ ] Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)

### 2021-08-23 to 2021-08-27
- [ ] Q1 PNPS Calls 
- [ ] Q1 SUS Calls 
- [ ] Direction Page Updates 
- [ ] Screening calls for Verify:PE PM
- [ ] Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅ Verify PE PM Acting Duties 

✅ Hiring Manager intervies for Verify:PE PM

✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅ Women at GitLab Mentorship Sync 

✅ OSS Impact for CI Minutes - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)

✅ Kenny Coverage tasks 

✅ Cheryl Coverage tasks 

✅ Weekly SaaS PI Update 

✅ Kick off Q3 Cross Stage Research 

### 2021-08-17 to 2021-08-21
✅ Q1 PNPS Calls 
✅ Q1 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Screening calls for Verify:PE PM
✅ Hiring Manager intervies for Verify:PE PM
✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅ Women at GitLab Mentorship Sync 
✅ Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅ OSS Impact for CI Minutes - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
✅ Kenny and Darren Coverage Tasks 
✅ 14.4 Planning 

### 2021-08-10 to 2021-08-14
✅ Q1 PNPS Calls 
✅ Q1 SUS Calls 
✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅ Verify PE PM Acting Duties 
✅ Screening calls for Verify:PE PM
✅ Hiring Manager intervies for Verify:PE PM
✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅ Women at GitLab Mentorship Sync 
✅ Pricing Handbook - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)
✅ OSS Impact for CI Minutes - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/210457)
✅ 14.3 Kick off - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/69)
✅ Heacount Reset for Infradev - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2934)
✅ Release posts for 14.2 
✅ Coverage tasks for Kenny, Darren, and Dov

### 2021-08-02 to 2021-08-06
✅  Q1 PNPS Calls 
✅  Q1 SUS Calls 
✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅  ] Verify PE PM Acting Duties 
✅  Screening calls for Verify:PE PM
✅  2H Verify Strategy Recording 
✅  Verify Quarterly Meeting with Verify PMs 
✅  Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅  August Verify  Process Refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/97)
✅  Women at GitLab Mentorship Sync 
✅  Weekly Ops SaaS Metrics Update - [Process](https://about.gitlab.com/handbook/product/categories/ops/#saas-reviews)
✅  14.3 Planning & Kick off - [Issue](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/69)
✅  Retro for include content in Releasing features - [Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/5125)

###  2021-07-26 to 2021-07-30
- [ ] Q1 PNPS Calls 
- [ ] Q1 SUS Calls
- [ ] 2H Verify Strategy Recording 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅  Verify PE PM Acting Duties 

✅  Screening calls for Verify:PE PM

✅  Verify Quarterly Meeting with Verify PMs 

✅   Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅   Enter Hive KRs into Ally 

✅   Hiring manager Interviews for PE PM 

✅   CDF Reviews - [Issues](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=verify%3Acdf)

✅   Internal Prep call for customer - [Agenda](https://docs.google.com/document/d/1JnLdTgnzUN8Q7JIz3Jm2jFgN5o8nsVrJQoo-Bbciqpw/)

✅   Women at GitLab Mentorship Sync 

### 2021-07-19 to 2021-07-23 
✅ Q1 PNPS Calls 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅ Verify PE PM Acting Duties 

✅ 14.3 Planning - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/69)

✅ Hydrate Needs Weight for 14.3 

✅ Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅ Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

✅ Custom CI scheudler scoping - [Issue](https://gitlab.com/gitlab-com/account-management/western-north-america/nvidia-group/nvidia-executive-requests/-/issues/5)

✅ Acq Support 

✅ Runner Core Scope Sequencing Support - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/94)

✅ Establish Verify Internships - [Issue 1](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1213) and [Issue 2](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1218)

✅ PI Page updates - [Weekly]() and [Monthly](https://gitlab.com/gitlab-com/Product/-/issues/2829)

- [ ] Q1 SUS Calls 
- [ ] Runner Survey 
- [ ] Direction Page Updates 

### 2021-07-12 to 2021-07-16 
- [ ] Q1 SUS Calls 
- [ ] Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
- [ ] Runner Survey 
- [ ] 14.3 Planning - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/69)

✅  Q1 PNPS Calls 

✅  Screening Calls for PE Product Manager 

✅ 
Verify CI PM Interim Duties 

✅  Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)

✅  OSS Focus Group - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/332614)

✅  Integration support - [Issue](https://gitlab.com/ubs-group1/ubs/ubs-global/-/issues/37)

✅  Ops Section Product Review 

✅ Pricing vs. Product Manager Responsibilities in Launch - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2787)

✅  Q3 Hive Kick-off - [Epic](https://gitlab.com/groups/gitlab-org/-/epics/6313) and [Agenda](https://docs.google.com/document/d/1S7d7SITpCyL5dnZjgG3TyWyWdMnjiI-P9k3OfyUnU2E/edit#heading=h.6w8tlk7jcvwy)

✅  Pipeline Execution Kick Off for 14.2 

### 2021-07-05 to 2021-07-09

✅  PTO for 4 days

✅ Verify Stage KRs - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/89)

###  2021-06-28 to 2021-07-02 
✅  Q1 PNPS Calls 

✅  Q1 SUS Calls 

✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅ Verify CI PM Interim Duties 

✅  Runner Survey 

✅ Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)

✅  Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

✅ Kenny Coverage - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2734)

✅  Verify Stage KRs - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/89)

✅  Integration support - [Issue](https://gitlab.com/ubs-group1/ubs/ubs-global/-/issues/37)

- [ ] Discuss Package/Verify Positioning with PMM [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2693)
- [ ] Product CDF Working Group - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2707)

### 2021-06-21 to 2021-06-25
✅   Q1 PNPS Calls 

✅   Q1 SUS Calls 

✅    Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅    Verify CI PM Interim Duties 

✅   Q2 Training for Verify - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/62)

✅   Q3 CI Goals - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/79)

✅    Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅    Verify Validation Backlog Review 

✅   SaaS Weekly Review PI [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2673) and OPs PI Review [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2689) 

- [ ] Runner Survey 
- [ ] Discuss Package/Verify Positioning with PMM [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2693)
- [ ] Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)
- [ ] Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

### 2021-06-14 to 2021-06-18 
- [ ] Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)
✅   Q1 PNPS Calls 
✅  Q1 SUS Calls 
✅   Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅  Verify CI PM Interim Duties 
✅   Q2 Training for Verify - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/62)
✅  Q3 CI Goals - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/79)
- [ ] Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)
✅  Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)
✅   Product Q2 KR Planning Retro [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2692)
✅   Kick off for PE 
✅  SaaS Weekly Review PI [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2673) and OPs PI Review [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2689) 

### 2021-06-07 to 2021-06-11
✅  PTO - [Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/2627)

### 2021-05-31 to 2021-06-04
✅  Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

✅  Q1 PNPS Calls 

✅  Q1 SUS Calls 

✅  Verify CI PM Interim Duties 

✅  Q2 Training for Verify - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/62)

✅  Q3 CI Goals - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/79)

✅  Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅  Pipeline Abuse Business Case - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2630)

✅ June Product Process Refresh - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/57)

✅ Transition Pipeline Abuse To Verify Product 

✅ Prep for PTO next Week

- [ ] Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
- [ ] Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)

### 2021-05-24 to 2021-05-28
✅   Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

✅   Q1 PNPS Calls 

✅   Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅  Verify CI PM Interim Duties 

✅  Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1385)

✅   Product CDF Working Group - [Issue List](https://gitlab.com/gitlab-com/Product/-/issues?label_name%5B%5D=wg%3Aproduct-cdf)

✅  Verify Stage Name Discussion - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2601)

✅  Engineering Allocation Support - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/330943)

✅  Milestone Planning for 14.1, 14.2 and beyond 

- [ ] Engineer Allocation Direction Pages - [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/82692)

- [ ] Q1 SUS Calls  

### 2021-05-03 to 2021-05-07

✅  Q4 PNPS Calls 

✅  Q4 SUS Calls 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅  Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

✅ Verify CI PM Interim Duties 

✅ Cryptomining Program Plan - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2499)

✅  Error Bugdets - MRs for PIs [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2485) and [AMA](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1021)

✅ Recruiting for Verify:CI Backfill - [Issue](https://gitlab.com/gl-talent-acquisition/req-intake/-/issues/1385)

- [ ] Verify Team Structure - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/59)
- [ ] United Model in Verify - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/60)

2021-04-26 to 2021-04-30 
✅ Q4 PNPS Calls 

✅ Q4 SUS Calls 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅ MRARR Contribution Support - [Issue](https://gitlab.com/ubs-group1/ubs/ubs-global/-/issues/37)

✅ Onboarding Buddy - [Issue](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2696)

✅ SAST + Code Quality - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2426)

✅  Verify CI PM Interim Duties 

✅ Ops PI Review - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2421)

✅  Verify Usage Funnel - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/48)

- [ ] Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

2021-04-12 to 2021-04-16
✅ Q4 PNPS Calls 

✅ Q4 SUS Calls 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

- [ ] MRARR Contribution Support - [Issue](https://gitlab.com/ubs-group1/ubs/ubs-global/-/issues/37)

✅ Establish Cross-Stage Process - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2334)

✅ Facilitating Cross Stage Adoption Research - [Dovetail](https://dovetailapp.com/projects/b34eab05-ab83-458d-86ec-fd9e9fcb9b44/data/b/22e362ad-e248-49ea-8181-cee27e69730b)

✅ How to approach bugs in CI - [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/76)

✅ Runner FAQs from Solution Architects - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2409)

✅ Is CI too Complex - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2408)

✅ Partnership Proposal Research - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2243)

2021-04-05 to 2021-04-09

✅] Q4 PNPS Calls 
✅] Q4 SUS Calls 
✅] Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅][CI Scaling Category](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78851)
✅] CI Template Community Discussion with Khatu - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/320698)
✅] Facilitating Cross Stage Adoption Research - [Dovetail](https://dovetailapp.com/projects/b34eab05-ab83-458d-86ec-fd9e9fcb9b44/data/b/22e362ad-e248-49ea-8181-cee27e69730b)
✅ [SaaS First Framework Feedback](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78684)

- [ ] Establish Cross-Stage Process - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2334)

### 2021-03-29 to 2021-04-02
✅  Q4 PNPS Calls 
✅  Q4 SUS Calls 
✅  Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 
✅  IDC Support - [Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4879)
✅  Error Budget Implementation - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2295)
✅  Dakota JTBD Reasearch - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1161)
✅  Recruiting for Shared KR - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1327)
✅  [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2317)
✅  [Product Process Refreshers](https://gitlab.com/gitlab-org/verify-stage/-/issues/51)

- [ ] CDF & Quantative Competencies - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2339)
- [ ] MRARR Contribution Support - [Issue](https://gitlab.com/ubs-group1/ubs/ubs-global/-/issues/37)


#### 2021-03-22 to 2021-03-26
✅  Q4 PNPS Calls 

✅  Q4 SUS Calls 

✅  IDC Support - [Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4879)

✅  Secrets Management Gartner Analyst Call - [Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4892)

✅  Next Generation CI Think Big 

✅  Error Budget Dashboard Support - [Issue](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/946#note_533590788)

✅  Dakota JTBD Reasearch - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1161)

- [ ]  [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/2317)

- [ ] Find & submit 8/10 clean up MRs in Handbook pages, Direction pages, etc. 

### 2021-03-08 to 2021-03-12
✅ Field Webcast with Parker Ennis - [Issue](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/110)

✅ Q4 PNPS Calls 

✅ Q4 SUS Calls 

✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

✅ Begin creating funnel - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/48) 

✅ Blueprint Architecture Support - [Issue](https://gitlab.com/gitlab-org/architecture/tasks/-/issues/5)

✅ IDC Support - [Issue](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4879)

✅ EBR Calls

✅ Prospect Calls 


- [ ] Call about use of Tekton with Propsects 
- [ ] Add team health review template - [Instructions](https://gitlab.com/gitlab-org/verify-stage/-/issues/44#note_516661285)

###  2021-03-01 to 2021-03-05
   ✅ Q4 PNPS Calls 

   ✅ Q4 SUS Calls 

   ✅ Send survey for Pipeline Visualization at Scale - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1336)

   ✅ Defining Outcomes Course 

   ✅ Create issue for funnel metrics in Verify - [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/48)

   ✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

   ✅  International Women's Day Panel - [Agenda](https://docs.google.com/document/d/1RXigQ1HLtLIgwVDgpc7XN_LJyTI4TdIh9l_arO3o1ik/edit#)

- [ ] Call about use of Tekton with Propsects 

###  2021-02-22 to 2021-02-26 

   ✅ Verify Team Day AMA - [Issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10809)

   ✅ Create Dovetail Template for Strategic Research - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1234)


   ✅ Record PMM + PM Positioning of Verify for Sales Enablement on CI - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2019)

   ✅ Explore Runner per Organization metrics 

   ✅ Defining Outcomes Course 

   ✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

   ✅  KR AMA's - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2095)

   ✅ Q4 PNPS Calls 

- [ ] Create issue for funnel metrics in Verify 

- [ ] Send survey for Pipeline Visualization at Scale - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1336)

###  2021-02-15 to 2021-02-19 
   ✅  Create the FAQ for CI move to Complete - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2163)
   
   ✅  Re-record Verify Stage 1H Strategy Review for Engineering Team & Field - [Issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10809)

   ✅  Schedule AMA for Field -  [Issue](https://gitlab.com/gitlab-com/Product/-/issues/1936)

   ✅ Defining Outcomes Course 

   ✅ Find & submit 10 clean up MRs in Handbook pages, Direction pages, etc. 

   ✅ Q4 PNPS Calls 

   ✅ QBRs 
   
   ✅ Reach out to user - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2087)

- [ ] KR AMA's - [Issue](https://gitlab.com/gitlab-com/Product/-/issues/2095)

- [ ] Send survey for Pipeline Visualization at Scale - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1336)

